# DropDown

Dropdown input control.<br/>Extends <b>mixin</b> `ComponentMixin`.

## Props

<!-- @vuese:DropDown:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|options|Dictionary of label `:string` and value `:any`|`Object`|`false`|-|
|label|Label displayed for dropdown|`String`|`false`|-|
|placeholder|Place holder for dropdown|`String`|`false`|-|

<!-- @vuese:DropDown:props:end -->


