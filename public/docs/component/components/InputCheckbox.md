# InputCheckbox

Checkbox group input control.<br/>Extends <b>mixin</b> `ComponentMixin`.

## Props

<!-- @vuese:InputCheckbox:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|options|Dictionary of label `:string` and value `:any`|`Object`|`false`|-|
|label|Label displayed for checkbox group control|`String`|`false`|-|

<!-- @vuese:InputCheckbox:props:end -->


