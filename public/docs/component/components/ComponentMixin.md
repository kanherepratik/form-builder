# ComponentMixin

Component common mixin with `v-model` implementation

## Props

<!-- @vuese:ComponentMixin:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|Value `:any` stored by the input component. Used as v-model|—|`false`|-|

<!-- @vuese:ComponentMixin:props:end -->


## Events

<!-- @vuese:ComponentMixin:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|`v-model` input event|`:any` value of input element|

<!-- @vuese:ComponentMixin:events:end -->


