# InputRadio

Radio group input control.<br/>Extends <b>mixin</b> `ComponentMixin`.

## Props

<!-- @vuese:InputRadio:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|options|Dictionary of label `:string` and value `:any`|`Object`|`false`|-|
|label|Label displayed for radio button group control|`String`|`false`|-|

<!-- @vuese:InputRadio:props:end -->


