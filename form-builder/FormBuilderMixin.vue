<script lang="ts">
import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import {
	IForm,
	IFormFieldTypeConstructor,
	IFormSection,
	IFormField,
	IFormAction,
	IFormEventMapSchema,
} from './shared/interfaces';
import FormRenderer from './FormRenderer.vue';
import { getTypeObject } from './shared/utils';
import { TYPE_MAP, FB_EVENTS_MAP } from './shared/constants';

@Component({
	components: {
		'form-renderer': FormRenderer,
	},
})
// Mixin to provide and export common form functionalities.
export default class FormBuilderMixin extends Vue {
	public formRendererRef: string = 'formRenderer';
	// Unique identifier for the form builder
	@Prop({type: String, required: true}) public id!: string;
	// Array of forms `:IForm` and properties to be rendered
	@Prop({ type: Array, default: [], required: true })
	public forms!: IForm[];
	// Dictionary of field id and data
	// tslint:disable:arrow-return-shorthand
	@Prop({ type: Object, default: (): {[key: string]: any} => { return {}; }}) public data!: { [key: string]: any };
	// `v-model` to store active form id
	@Prop(String) protected value!: string;
	// Dictionary of input type names `:string` and classes `:IFormFieldTypeConstructor`
	// to represent input components.
	// <br/><i><small>Note: Classes must implement `:IFormFieldType` and have props and events
	// required by the input component they represent.</small></i>
	@Prop({ type: Object, default: () => TYPE_MAP }) protected typeMap!: {
		[key: string]: IFormFieldTypeConstructor;
	};
	// Displays an "Update and review" button on steps (except the last step), once all the forms are submitted.
	@Prop({ type: Boolean, default: true })
	protected showUpdateButton!: boolean;
	private eventMap: { [formBuilderId: string]: IFormEventMapSchema } = {};

	// @vuese
	/**
	 * Fetches the reference element of form field.
	 * <br/><b>Returns</b> reference element or `null` if fieldId does not exist.
	 * @arg <b>fieldId</b>`:string`<br/>Field id specified in the form json.
	 */
	public getFieldRef(fieldId: string): any {
		const ref: any = this.$refs[this.formRendererRef];
		if (ref) {
			return ref.getFieldRef(fieldId);
		}
		return null;
	}
	// @vuese
	/**
	 * `getter`<br/><b>Returns</b> index `:number` of the currently active form
	 */
	public get activeFormIndex(): number {
		const index: number = this.forms.findIndex(
			(form: IForm) => form.formId === this.activeFormId && !form.formHidden,
		);
		if (index === -1) {
			return this.firstVisibleFormIndex === -1 ? 0 : this.firstVisibleFormIndex;
		}
		return index;
	}
	// @vuese
	/**
	 * `getter`<br/><b>Returns</b> currently active form object `:IForm`
	 */
	public get activeForm(): IForm {
		return this.forms[this.activeFormIndex === -1 ? 0 : this.activeFormIndex];
	}
	// @vuese
	/**
	 * `getter`<br/><b>Returns</b> index of first visible form `:number`
	 */
	public get firstVisibleFormIndex(): number {
		const index: number = this.forms.findIndex((form: IForm) => !form.formHidden);
		return index === -1 ? 0 : index;
	}
	// @vuese
	/**
	 * `getter`<br/><b>Returns</b> index of last visible form `:number`
	 */
	public get lastVisibleFormIndex(): number {
		const reverseForms: IForm[] = [...this.forms].reverse();
		const reverseFirstFormIndex: number = reverseForms.findIndex((form: IForm) => !form.formHidden);
		return reverseFirstFormIndex === -1 ?
			(this.forms.length - 1)
			: Math.abs(reverseFirstFormIndex - (this.forms.length - 1));
	}

	// @vuese
	/**
	 * Accepts form index `:number` and <br/><b>Returns</b> index of previous visible form `:number`
	 */
	public previousFormIndex(formIndex: number): number {
		const reverseForms: IForm[] = [...this.forms].reverse();
		const reverseFormIndex: number = Math.abs(formIndex - (this.forms.length - 1));
		const reverseNextFormIndex: number = reverseForms.findIndex(
			(form: IForm, index: number) => index > reverseFormIndex && !form.formHidden,
		);
		return reverseNextFormIndex === -1 ? formIndex : Math.abs(reverseNextFormIndex - (this.forms.length - 1));
	}
	// @vuese
	/**
	 * Accepts form index `:number` and <br/><b>Returns</b> index of next visible form `:number`
	 */
	public nextFormIndex(formIndex: number): number {
		const nextIndex: number =  this.forms.findIndex(
			(form: IForm, index: number) => index > formIndex && !form.formHidden,
		);
		return nextIndex === -1 ? formIndex : nextIndex;
	}

	protected onSubmit(): void {
		// Fired when form submit button is clicked
		this.$emit('submit');
	}

	protected get isUpdateButtonVisible(): boolean {
		return (
			this.activeFormIndex < this.lastVisibleFormIndex &&
			this.forms.findIndex((form: IForm) => !form.formIsSubmitted && !form.formHidden) === -1
		);
	}
	protected onUpdateAndReview(): void {
		// Fired when form "Update and review" button is clicked.
		// @arg `:IForm`<br/>Currently active form
		this.$emit('update', this.activeForm);
	}

	protected get activeFormId(): string {
		return this.value;
	}
	protected set activeFormId(activeFormId: string) {
		this.setActiveForm(activeFormId);
		// `v-model` input event.
		// @arg `:string`<br/>Form id of currently active form
		this.$emit('input', activeFormId);
	}

	private created(): void {
		this.eventMap = (this.$parent as any)[FB_EVENTS_MAP] || {};
		// Invalidate field if type does not exist
		this.initializeFormSchema();

		// Set active form
		this.setActiveForm(this.value);
	}
	private setActiveForm(activeFormId: string): void {
		const index: number = this.forms.findIndex(
			(form: IForm) => form.formId === activeFormId && !form.formHidden,
		);
		// Error message in case of invalid form id
		if (activeFormId && index === -1) {
			// tslint:disable-next-line:no-console
			console.error(
				'Form Id: "'
				+ activeFormId
				+ '" cannot be active as it is either hidden or does not exist in the form schema',
			);
		}
		if (
			activeFormId &&
			index > -1 &&
			(index === this.firstVisibleFormIndex ||
				this.forms[index].formIsSubmitted ||
				this.forms[this.previousFormIndex(index)].formIsSubmitted)
		) {
			this.forms.forEach((form: IForm) => {
				form.formIsActive = false;
				if (form.formId === activeFormId) {
					form.formIsActive = true;
				}
			});
		} else {
			const incompleteFormIndex: number = this.forms.findIndex(
				(form: IForm) => !form.formIsSubmitted && !form.formHidden,
			);
			if (incompleteFormIndex > -1) {
				// Make first un-submitted and visible form active
				this.activeFormId = this.forms[incompleteFormIndex].formId;
			} else {
				let activeIndex: number = this.forms.findIndex(
					(form: IForm) => form.formIsActive === true && !form.formHidden,
				);
				if (activeIndex === -1) {
					// Make last submitted form active if all forms are submitted and no form is active
					activeIndex = this.lastVisibleFormIndex;
				}
				this.activeFormId = this.forms[activeIndex].formId;
			}
		}
	}
	@Watch('value')
	private onValueChange(newValue: string): void {
		this.setActiveForm(newValue);
	}
	@Watch('forms')
	private onFormObjectChange(): void {
		this.initializeFormSchema();
	}
	private instantiateField(component: IFormFieldTypeConstructor, field: IFormField) {
		if (!field.fieldObject) {
			field.fieldObject = getTypeObject(component, field);
		}
		if (field.fieldObject) {
			field.fieldObject.events = {};
			if (this.eventMap[this.id]) {
				// check for events for all fields
				for (const eventName of Object.keys(this.eventMap[this.id].all)) {
					const excludedIds: string[] | undefined = this.eventMap[this.id].all[eventName].excludedIds;
					if (!excludedIds || (excludedIds && excludedIds.length && excludedIds.indexOf(field.fieldId) === -1)) {
						// add event to field
						const event: IFormAction = this.eventMap[this.id].all[eventName].action;
						field.fieldObject.events[eventName] = event.fn.bind(this.$parent, {
							field,
							data: event.args,
						});
					}
				}
				// check for events for type
				if (this.eventMap[this.id].types[field.fieldType]) {
					for (const eventName of Object.keys(this.eventMap[this.id].types[field.fieldType])) {
						const excludedIds: string[] | undefined = this.eventMap[this.id].types[field.fieldType][eventName].excludedIds;
						if (!excludedIds || (excludedIds && excludedIds.length && excludedIds.indexOf(field.fieldId) === -1)) {
							// add event to field
							const event: IFormAction = this.eventMap[this.id].types[field.fieldType][eventName].action;
							field.fieldObject.events[eventName] = event.fn.bind(this.$parent, {
								field,
								data: event.args,
							});
						}
					}
				}
				// check for individual field event
				// console.log('parent', this.$parent);
				const eventList: {[eventName: string]: IFormAction} | undefined = this.eventMap[this.id].fields[field.fieldId];
				if (eventList) {
					for (const key of Object.keys(eventList)) {
						field.fieldObject.events[key] = eventList[key].fn.bind(this.$parent, {
							field,
							data: eventList[key].args,
						});
					}
				}
			}
		}
	}
	private initializeFormSchema(): void {
		if (this.forms.length) {
			this.forms.forEach((form: IForm) => {
				form.formSections.forEach((section: IFormSection) => {
					section.sectionFields.forEach((field: IFormField) => {
						if (this.typeMap[field.fieldType]) {
							this.instantiateField(this.typeMap[field.fieldType], field);
						} else {
							field.fieldIsInvalid = true;
							// Throw error if field type does not exist
							// tslint:disable-next-line:no-console
							console.error(
								'Field "' + field.fieldId + '", '
								+ 'has fieldType either missing or not registered in typeMap '
								+ '(check component documentation)');
							// tslint:disable-next-line:no-console
							console.error('Type names allowed/registered in typeMap: ', Object.keys(this.typeMap));
						}
					});
				});
			});
		}
		// Triggers after every time the form and its objects are initialized
		this.$emit('formInitialized');
	}
}
</script>
