import {
	IFormAction,
	IFormFieldTypeConstructor,
	IFormEventMapSchema,
	IFormFormatterMapSchema,
} from './interfaces';
import {
	InputTextType,
	InputRadioType,
	DropDownType,
	InputCheckboxType,
	InputTextareaType,
} from '../components/shared/type';
import { InputControlTypes } from '../components/shared/enums';

/**
 * Regular expression used to detect $$value$$ as a placeholder for form data
 */
export const valueExp: RegExp = /\$\$value\$\$/g;
/**
 * Name of `Collapsible` component
 */
export const COLLAPSIBLE_NAME: string = 'collapsible';
/**
 * Default type map, with default input components
 */
export const TYPE_MAP: {[key: string]: IFormFieldTypeConstructor} = {
	[InputControlTypes.Text]: InputTextType,
	[InputControlTypes.Radio]: InputRadioType,
	[InputControlTypes.Dropdown]: DropDownType,
	[InputControlTypes.Checkbox]: InputCheckboxType,
	[InputControlTypes.Textarea]: InputTextareaType,
};

/**
 * Variable name which stores event function map
 */
export const FB_EVENTS_MAP: string = '__formBuilderEvents';
/**
 * Variable name which stores formatter function map for fields
 */
export const FB_FIELD_REVIEW_FORMATTER_MAP: string = '__formBuilderFieldReviewFormatters';
/**
 * Variable name which stores formatter function map for section headers
 */
export const FB_SECTION_REVIEW_FORMATTER_MAP: string = '__formBuilderSectionReviewFormatters';
/**
 * Variable name which stores formatter function map for form headers
 */
export const FB_FORM_REVIEW_FORMATTER_MAP: string = '__formBuilderFormReviewFormatters';
