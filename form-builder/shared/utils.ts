import { IForm, IFormField, IFormSection, IFormEventMapSchema, IFormFormatterMapSchema } from './interfaces';
import { Vue } from 'vue-property-decorator';
import {
	FB_EVENTS_MAP,
	FB_FIELD_REVIEW_FORMATTER_MAP,
	FB_SECTION_REVIEW_FORMATTER_MAP,
	FB_FORM_REVIEW_FORMATTER_MAP,
} from './constants';

/**
 * Initializes custom type component object
 * @param type Class type of which object is to be initialized
 * @param field Object data to initialize with
 * @returns Object of the `type` param
 * @typeparam T Custom type component expected
 */
export const getTypeObject = <T>(
	type: new (field: any) => T,
	field: any,
): T => {
	return new type(field);
};

/**
 * Determines if a form is accessible and can be navigated to.
 * A form is accessible when it is placed before the active form.
 * @param index Index position of a form which is within a form array
 * @param activeIndex Index position of the active form which is within a form array
 */
export const canNavigate = (index: number, activeIndex: number, isFormHidden: boolean): boolean => {
	return !isFormHidden && (index < activeIndex);
};

/**
 * Attaches a dictionary of events to a field
 * @param forms Forms array
 * @param fieldId Field id to attach events to
 * @param events Dictionary of event names and functions to be triggered on those events
 */
export const attachEvents = (
	forms: IForm[],
	fieldId: string,
	// tslint:disable-next-line
	events: { [key: string]: Function },
): void => {
	for (const form of forms) {
		for (const section of form.formSections) {
			for (const field of section.sectionFields) {
				if (field.fieldId === fieldId && field.fieldObject) {
					field.fieldObject.events = events;
				}
			}
		}
	}
};

/**
 * Attaches a single event to a field
 * @param forms Forms array
 * @param fieldId Field id to attach event to
 * @param event Event name
 * @param fn Function to be triggered on event
 * @param args Object having arguments to be passed to the `fn`
 * @param bindTo Context to bind to, mostly passed `this`
 */
export const attachEvent = (
	forms: IForm[],
	fieldId: string,
	event: string,
	// tslint:disable-next-line
	fn: Function,
	args: object,
	bindTo: Vue,
): void => {
	for (const form of forms) {
		for (const section of form.formSections) {
			for (const field of section.sectionFields) {
				if (field.fieldId === fieldId && field.fieldObject) {
					field.fieldObject.events[event] = fn.bind(bindTo, {
						field,
						...args,
					});
				}
			}
		}
	}
};

/**
 * Fetches form field object from the forms array
 * @param forms Forms array
 * @param fieldId Field id of [[IFormField]] to be fetched
 * @returns [[IFormField]] object if found and `undefined` if no such field, with field id provided, is found.
 */
export const getField = (
	forms: IForm[],
	fieldId: string,
): IFormField | undefined => {
	let targetField!: IFormField;
	forms.forEach((form: IForm) => {
		form.formSections.forEach((section: IFormSection) => {
			const result: IFormField | undefined = section.sectionFields.find(
				(field: IFormField) => field.fieldId === fieldId,
			);
			if (result) {
				targetField = result;
				return;
			}
		});
	});
	return targetField;
};

/**
 * Decorator, which defines function as event call back of a field.
 * Decorated function will receive formEventArgs [[IFormEventArgs]] object as the first parameter
 * and then data passed by event
 * @param formBuilderId Form builder id
 * @param fieldIds List of field ids to apply the event to
 * @param event Event name
 * @param parameters Optional parameter
 */
export const formEvent = (formBuilderId: string, fieldIds: string[], event: string, parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_EVENTS_MAP]) {
				target.__proto__[FB_EVENTS_MAP] = {};
			}
			const eventMap: {
				[formBuilderId: string]: IFormEventMapSchema,
			} = target.__proto__[FB_EVENTS_MAP];

			if (!eventMap[formBuilderId]) {
				eventMap[formBuilderId] = {
					fields: {},
					types: {},
					all: {},
				};
			}
			for (const fieldId of fieldIds) {
				if (!eventMap[formBuilderId].fields[fieldId]) {
					eventMap[formBuilderId].fields[fieldId] = {};
				}
				if (!eventMap[formBuilderId].fields[fieldId][event]) {
					eventMap[formBuilderId].fields[fieldId][event] = {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					};
				}
				if (typeof descriptor.value === 'function') {
					eventMap[formBuilderId].fields[fieldId][event].fn = descriptor.value;
					eventMap[formBuilderId].fields[fieldId][event].args = parameters;
				}
			}
		};
};

/**
 * Decorator, which defines function as event call back for fields of a type
 * Decorated function will receive formEventArgs [[IFormEventArgs]] object as the first parameter
 * and then data passed by event
 * @param formBuilderId Form builder id
 * @param fieldTypes Event will be applied to the fields of type in this list
 * @param event Event name
 * @param excludedFieldIds List of field ids to be excluded, which fall under the type
 * @param parameters Optional parameter
 */
export const formEventOfType = (
	formBuilderId: string,
	fieldTypes: string[],
	event: string,
	excludedFieldIds: string[],
	parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_EVENTS_MAP]) {
				target.__proto__[FB_EVENTS_MAP] = {};
			}
			const eventMap: {
				[formBuilderId: string]: IFormEventMapSchema,
			} = target.__proto__[FB_EVENTS_MAP];

			if (!eventMap[formBuilderId]) {
				eventMap[formBuilderId] = {
					fields: {},
					types: {},
					all: {},
				};
			}
			for (const fieldType of fieldTypes) {
				if (!eventMap[formBuilderId].types[fieldType]) {
					eventMap[formBuilderId].types[fieldType] = {};
				}
				if (!eventMap[formBuilderId].types[fieldType][event]) {
					eventMap[formBuilderId].types[fieldType][event] = {
						action: {
							// tslint:disable-next-line
							fn: () => {},
							args: {},
						},
					};
					if (excludedFieldIds && excludedFieldIds.length) {
						eventMap[formBuilderId].types[fieldType][event].excludedIds = [...excludedFieldIds];
					}
				}
				if (typeof descriptor.value === 'function') {
					eventMap[formBuilderId].types[fieldType][event].action.fn = descriptor.value;
					eventMap[formBuilderId].types[fieldType][event].action.args = parameters;
				}
			}
		};
};

/**
 * Decorator which defines function as event call back for all fields in form
 * Decorated function will receive formEventArgs [[IFormEventArgs]] object as the first parameter
 * and then data passed by event
 * @param formBuilderId Form builder id
 * @param event Event name
 * @param excludedFieldIds List of field ids to be excluded
 * @param parameters Optional parameter
 */
export const formEventForAll = (
	formBuilderId: string,
	event: string,
	excludedFieldIds: string[],
	parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_EVENTS_MAP]) {
				target.__proto__[FB_EVENTS_MAP] = {};
			}
			const eventMap: {
				[formBuilderId: string]: IFormEventMapSchema,
			} = target.__proto__[FB_EVENTS_MAP];

			if (!eventMap[formBuilderId]) {
				eventMap[formBuilderId] = {
					fields: {},
					types: {},
					all: {},
				};
			}
			if (!eventMap[formBuilderId].all[event]) {
				eventMap[formBuilderId].all[event] = {
					action: {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					},
				};
			}
			if (excludedFieldIds && excludedFieldIds.length) {
				eventMap[formBuilderId].all[event].excludedIds = [...excludedFieldIds];
			}
			if (typeof descriptor.value === 'function') {
				eventMap[formBuilderId].all[event].action.fn = descriptor.value;
				eventMap[formBuilderId].all[event].action.args = parameters;
			}
		};
};

/**
 * Decorator which defines function as formatter function for a field
 * Decorated function will receive optional data, field value, field [[IFormField]] object,
 * section [[IFormSection]] object, form [[IForm]] and forms [[IForm]][] as parameters
 * @param formReviewId Form Review id
 * @param fieldIds List of field ids
 * @param parameters Optional parameter
 */
export const reviewFieldFormatter = (formReviewId: string, fieldIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			for (const fieldId of fieldIds) {
				if (!formatterMap[formReviewId].fields[fieldId]) {
					formatterMap[formReviewId].fields[fieldId] = {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					};
				}
				if (typeof descriptor.value === 'function') {
					formatterMap[formReviewId].fields[fieldId].fn = descriptor.value;
					formatterMap[formReviewId].fields[fieldId].args = parameters;
				}
			}
		};
};

/**
 * Decorator which defines function as formatter function of fields of a type
 * Decorated function will receive optional data, field value, field [[IFormField]] object,
 * section [[IFormSection]] object, form [[IForm]] and forms [[IForm]][] as parameters
 * @param formReviewId Form Review id
 * @param fieldTypes List of field types
 * @param excludedFieldIds List of field ids to be excluded
 * @param parameters Optional parameter
 */
export const reviewFieldFormatterOfType = (
	formReviewId: string,
	fieldTypes: string[],
	excludedFieldIds: string[],
	parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			for (const fieldType of fieldTypes) {
				if (!formatterMap[formReviewId].types[fieldType]) {
					formatterMap[formReviewId].types[fieldType] = {
						action: {
							// tslint:disable-next-line
							fn: () => {},
							args: {},
						},
					};
				}
				if (excludedFieldIds && excludedFieldIds.length) {
					formatterMap[formReviewId].types[fieldType].excludedIds = [...excludedFieldIds];
				}
				if (typeof descriptor.value === 'function') {
					formatterMap[formReviewId].types[fieldType].action.fn = descriptor.value;
					formatterMap[formReviewId].types[fieldType].action.args = parameters;
				}
			}
		};
};

/**
 * Decorator which defines function as formatter function for all fields
 * Decorated function will receive optional data, field value, field [[IFormField]] object,
 * section [[IFormSection]] object, form [[IForm]] and forms [[IForm]][] as parameters
 * @param formReviewId Form Review id
 * @param excludedFieldIds List of field ids excluded
 * @param parameters Optional parameter
 */
export const reviewFieldFormatterForAll = (formReviewId: string, excludedFieldIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_FIELD_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			if (!formatterMap[formReviewId].all) {
				formatterMap[formReviewId].all = {
					action: {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					},
				};
			}
			if (excludedFieldIds && excludedFieldIds.length) {
				(formatterMap[formReviewId].all as any).excludedIds = [...excludedFieldIds];
			}
			if (typeof descriptor.value === 'function') {
				(formatterMap[formReviewId].all as any).action.fn = descriptor.value;
				(formatterMap[formReviewId].all as any).action.args = parameters;
			}
		};
};

/**
 * Decorator which defines function as formatter function for list of sections
 * Decorated function will receive optional data, section [[IFormSection]] object,
 * form [[IForm]] and forms [[IForm]][]  as parameters
 * @param formReviewId Form Review Id
 * @param sectionIds List of section ids
 * @param parameters Optional parameter
 */
export const reviewSectionFormatter = (formReviewId: string, sectionIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			for (const sectionId of sectionIds) {
				if (!formatterMap[formReviewId].sections[sectionId]) {
					formatterMap[formReviewId].sections[sectionId] = {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					};
				}
				if (typeof descriptor.value === 'function') {
					formatterMap[formReviewId].sections[sectionId].fn = descriptor.value;
					formatterMap[formReviewId].sections[sectionId].args = parameters;
				}
			}
		};
};

/**
 * Decorator which defines function as formatter function for all sections
 * Decorated function will receive optional data, section [[IFormSection]] object,
 * form [[IForm]] and forms [[IForm]][]  as parameters
 * @param formReviewId Form Review id
 * @param excludedSectionIds List of section ids to be excluded
 * @param parameters Optional parameter
 */
export const reviewSectionFormatterForAll = (formReviewId: string, excludedSectionIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_SECTION_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			if (!formatterMap[formReviewId].all) {
				formatterMap[formReviewId].all = {
					action: {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					},
				};
			}
			if (excludedSectionIds && excludedSectionIds.length) {
				(formatterMap[formReviewId].all as any).excludedIds = [...excludedSectionIds];
			}
			if (typeof descriptor.value === 'function') {
				(formatterMap[formReviewId].all as any).action.fn = descriptor.value;
				(formatterMap[formReviewId].all as any).action.args = parameters;
			}
		};
};

/**
 * Decorator which defines function as formatter function for list of forms
 * Decorated function will receive optional data,
 * form [[IForm]] and forms [[IForm]][]  as parameters
 * @param formReviewId Form Review Id
 * @param formIds List of form ids
 * @param parameters Optional parameter
 */
export const reviewFormFormatter = (formReviewId: string, formIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			for (const formId of formIds) {
				if (!formatterMap[formReviewId].forms[formId]) {
					formatterMap[formReviewId].forms[formId] = {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					};
				}
				if (typeof descriptor.value === 'function') {
					formatterMap[formReviewId].forms[formId].fn = descriptor.value;
					formatterMap[formReviewId].forms[formId].args = parameters;
				}
			}
		};
};

/**
 * Decorator which defines function as formatter function for all forms
 * Decorated function will receive optional data,
 * form [[IForm]] and forms [[IForm]][]  as parameters
 * @param formReviewId Form Review id
 * @param excludedFormIds List of section ids to be excluded
 * @param parameters Optional parameter
 */
export const reviewFormFormatterForAll = (formReviewId: string, excludedFormIds: string[], parameters: any = null)
	: ((target: any, propertyKey: string, descriptor: PropertyDescriptor) => void) => {
		return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
			// Add map to prototype
			if (!target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP]) {
				target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP] = {};
			}
			const formatterMap: {
				[formReviewerId: string]: IFormFormatterMapSchema,
			} = target.__proto__[FB_FORM_REVIEW_FORMATTER_MAP];

			if (!formatterMap[formReviewId]) {
				formatterMap[formReviewId] = {
					fields: {},
					sections: {},
					forms: {},
					types: {},
				};
			}
			if (!formatterMap[formReviewId].all) {
				formatterMap[formReviewId].all = {
					action: {
						// tslint:disable-next-line
						fn: () => {},
						args: {},
					},
				};
			}
			if (excludedFormIds && excludedFormIds.length) {
				(formatterMap[formReviewId].all as any).excludedIds = [...excludedFormIds];
			}
			if (typeof descriptor.value === 'function') {
				(formatterMap[formReviewId].all as any).action.fn = descriptor.value;
				(formatterMap[formReviewId].all as any).action.args = parameters;
			}
		};
};
